package no.ntnu.idatt2001;

/**
 * The type Playing card.
 */
public class PlayingCard {

    private final char suit; // 'C'=clubs, 'S'=spade, 'H'=heart, 'D'=diamonds
    private final int face; // a number between 1 and 13


    /**
     * Instantiates a new Playing card.
     *
     * @param suit the suit
     * @param face the face
     */
    public PlayingCard(char suit, int face) {
        this.suit = suit;
        this.face = face;
    }


    /**
     * Gets as string.
     * A two of diamonds will be returned as "D2"
     *
     * @return the as string
     */
    public String getAsString() {
        return String.format("%s%s", suit, face);
    }


    /**
     * Gets suit.
     *
     * @return the suit
     */
    public char getSuit() {
        return suit;
    }


    /**
     * Gets face.
     *
     * @return the face
     */
    public int getFace() {
        return face;
    }
}