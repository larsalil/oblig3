package no.ntnu.idatt2001;

import java.util.Random;
import java.util.ArrayList;

/**
 * The type Deck of cards.
 */
public class DeckOfCards {


    private ArrayList<PlayingCard> deck = new ArrayList<>();
    private Random rand = new Random();

    /**
     * Instantiates a new standard 52-card Deck of cards.
     */
    public DeckOfCards() {
        char[] suits = {'C', 'S', 'H', 'D'};
        int[] faces = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        for (char suit : suits) {
            for (int face : faces) {
                deck.add(new PlayingCard(suit, face));
            }
        }
    }

    /**
     * Creates a n-size HandOfCards.
     * Removes the card from the deck as it is dealt.
     *
     * @param n the n
     * @return the hand of cards
     */
    public HandOfCards dealHand(int n) {
        HandOfCards hand = new HandOfCards();
        for (int i = 1; i <= n; i++) {
            int bound = deck.size();
            int number = rand.nextInt(bound);
            hand.addCard(deck.remove(number));
        }
        return hand;
    }
}
