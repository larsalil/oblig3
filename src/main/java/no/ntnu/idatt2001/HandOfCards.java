package no.ntnu.idatt2001;

import java.util.ArrayList;

/**
 * The type Hand of cards.
 */
public class HandOfCards {
    private ArrayList<PlayingCard> hand = new ArrayList<>();

    /**
     * Gets hand.
     *
     * @return the hand
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }

    /**
     * Gets card at input index.
     *
     * @param index the index
     * @return the card at index
     */
    public PlayingCard getCardAt(int index) {
        return hand.get(index);
    }

    /**
     * Add card.
     *
     * @param card the card
     */
    public void addCard(PlayingCard card) {
        hand.add(card);
    }


    /**
     * Sum faces.
     *
     * @return the string
     */
    public String sumFaces() {
        return String.valueOf(hand.stream().map(PlayingCard::getFace)
                                           .reduce(Integer::sum).orElseThrow());
    }

    /**
     * Print hearts string
     *
     * @return the string
     */
    public String printHearts() {
        StringBuilder out = new StringBuilder();
        hand.stream().filter(p -> p.getSuit() == 'H')
                     .forEach(p -> out.append(p.getAsString()).append(" "));
        if (out.toString().equals("")) {
            return "No hearts on hand";
        } else {
            return out.toString();
        }

    }

    /**
     * Hand contains queen of spades boolean
     *
     * @return the boolean
     */
    public boolean handContainsQueenOfSpades() {
        return hand.stream().anyMatch(playingCard -> playingCard.getAsString().equals("S12"));
    }

    /**
     * Hand contains 5 flush boolean.
     *
     * @return the boolean
     */
    public boolean handContains5Flush() {
        return hand.stream().allMatch(n -> n.getSuit() == hand.get(0).getSuit());
    }


}
