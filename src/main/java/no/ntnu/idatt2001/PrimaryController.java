package no.ntnu.idatt2001;

import java.io.FileNotFoundException;
import java.util.Objects;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class PrimaryController {

    //Create deck on start-up
    private final DeckOfCards deck = new DeckOfCards();
    public Text sumText;
    public Text heartsText;
    public Text queenOfSpadesText;
    public Text flushText;
    private HandOfCards hand;


    //The 5 cards to be displayed
    @FXML
    public ImageView cardView1;

    @FXML
    public ImageView cardView2;

    @FXML
    public ImageView cardView3;

    @FXML
    public ImageView cardView4;

    @FXML
    public ImageView cardView5;

    /**
     * On deal hand button click.
     * Creates a 5-size HandOfCards, then attempts to display them.
     */
    public void onDealHandButtonClick() {

        //Dealing hand
        hand = deck.dealHand(5);
        try {
            displayCards();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Checks the hand
        sumText.setText(hand.sumFaces());
        heartsText.setText(hand.printHearts());
        if (hand.handContainsQueenOfSpades()) {
            queenOfSpadesText.setText("True");
        } else {
            queenOfSpadesText.setText("False");
        }
        if (hand.handContains5Flush()) {
            flushText.setText("True");
        } else {
            flushText.setText("False");
        }

    }

    /**
     * Display 5 cards
     *
     * @throws FileNotFoundException the file not found exception
     */
    public void displayCards() throws FileNotFoundException{
        cardView1.setImage(new Image(Objects.requireNonNull(this.getClass().getResourceAsStream("/no/ntnu/idatt2001/playingCards/" +
                hand.getCardAt(0).getAsString() + ".png"))));
        cardView2.setImage(new Image(Objects.requireNonNull(this.getClass().getResourceAsStream("/no/ntnu/idatt2001/playingCards/" +
                hand.getCardAt(1).getAsString() + ".png"))));
        cardView3.setImage(new Image(Objects.requireNonNull(this.getClass().getResourceAsStream("/no/ntnu/idatt2001/playingCards/" +
                hand.getCardAt(2).getAsString() + ".png"))));
        cardView4.setImage(new Image(Objects.requireNonNull(this.getClass().getResourceAsStream("/no/ntnu/idatt2001/playingCards/" +
                hand.getCardAt(3).getAsString() + ".png"))));
        cardView5.setImage(new Image(Objects.requireNonNull(this.getClass().getResourceAsStream("/no/ntnu/idatt2001/playingCards/" +
                hand.getCardAt(4).getAsString() + ".png"))));
    }
}